import json


def read_data():
    with open("data.json", "r") as f:
        return json.load(f)


def write_data(src):
    with open("data.json", "w") as f:
        json.dump(src, f)


data = read_data()

countTry = 3
loginIsTrue = False
passwordIsTrue = False

userIsBlocked = False
logged = False

while countTry > 0 and not userIsBlocked and not logged:
    if loginIsTrue:
        password = input("Enter password: ")
        if data[login] == password:
            passwordIsTrue = True
            logged = True
        else:
            print("\nWrong password!")
    else:
        login = input("Enter login: ")
        if login in data:
            loginIsTrue = True
            countTry = 4
        else:
            print("\nWrong login!")
    countTry -= 1
    if countTry == 0:
        userIsBlocked = True

if logged:
    print("Успех!")
else:
    print("Аккаунт заблокрован.")

