class Advert:
    def __init__(self, advertstor_name: str, advert_duration: int, number_impressions: int):
        self.advertstor_name = advertstor_name
        self.advert_duration = advert_duration
        self.number_impressions = number_impressions

    @property
    def cost(self):
        return self.number_impressions * 2000

    def set_advertstor_name(self, new_advertstor_name: str) -> None:
        self.advertstor_name = new_advertstor_name

    def set_number_impressions(self, new_number_impressions: int) -> None:
        self.number_impressions = new_number_impressions

    def __str__(self):
        return f"Advertstor: {self.advertstor_name}; " \
               f"Number impressions: {self.number_impressions}; " \
               f"Cost: {self.cost}"
