from lab.lab2.dao.UserDAO import get_user, update_user, create_user


def register_user(login: str, password: str) -> bool:
    if len(login) > 0 and len(password) > 0 and not get_user(login):
        create_user({"login": login,
                   "password": password,
                   "countEnterTry": 0,
                   "isBlocked": False})
        return True
    return False


def check_login(login: str) -> bool:
    user = get_user(login)
    return user and not user["isBlocked"]


def check_user(login: str, password: str) -> bool:
    user = get_user(login)

    if user and not user["isBlocked"]:
        if user["password"] == password:
            user["countEnterTry"] = 0
            update_user(user)
            return True
        else:
            user["countEnterTry"] += 1
            if user["countEnterTry"] >= 3:
                user["countEnterTry"] = 0
                user["isBlocked"] = True
            update_user(user)
    return False
