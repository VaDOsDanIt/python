from lab.lab2.service.Auth import check_login, check_user, register_user


def print_menu(logged: bool) -> None:
    if logged:
        print("\n1. Secret item menu 1")
        print("2. Secret item menu 2")
        print("3. Secret item menu 3")
        print("4. Logout")
    else:
        print("\n1. Register")
        print("2. Login")
    print("0. Exit")


def register_dialog() -> bool:
    login = input("Login: ")
    password = input("Password: ")
    return register_user(login, password)


def login_dialog() -> dict:
    countTry = 0
    loginIsTrue = False
    blocked = False

    while not loginIsTrue and not blocked:
        countTry += 1

        login = input("Enter login: ")
        loginIsTrue = check_login(login)
        if countTry == 3 and not loginIsTrue:
            blocked = True

    countTry = 0
    while loginIsTrue and not blocked:
        countTry += 1

        password = input(login + ", enter password: ")
        if check_user(login, password):
            return {
                "success": True,
                "login": login
            }
        if countTry == 3:
            countTry = 0
            blocked = True
    return {"success": False}


def input_int_value(message: str, fallback: int) -> int:
    try:
        return int(input(message))
    except ValueError:
        return fallback


def console_init() -> None:
    cmd = None
    userIsLogged = False
    userLogin = ""

    while cmd != 0:
        if userIsLogged:
            print("Hello! " + userLogin)
        else:
            print("Hello! User!")

        print_menu(userIsLogged)
        cmd = input_int_value("Enter command key: ", -1)

        if userIsLogged:
            if cmd == 1:
                print("\nSecret page 1")
            elif cmd == 2:
                print("\nSecret page 2")
            elif cmd == 3:
                print("\nSecret page 3")
            elif cmd == 4:
                userIsLogged = False
            elif cmd == 0:
                print("Exit...")
                userIsLogged = False
            else:
                print("Wrong operation!")
        else:
            if cmd == 1:
                print("\nRegister")
                register = register_dialog()
                if register:
                    print("Success!")
                else:
                    print("Login or password is wrong! Or login already uses!")
            elif cmd == 2:
                print("\nLogin")
                result = login_dialog()
                userIsLogged = result["success"]
                if result["success"]:
                    userLogin = result["login"]
                if userIsLogged:
                    print("Success!")
                else:
                    print("Login or password is wrong! Or your account is blocked!")
            elif cmd == 0:
                print("Exit...")
            else:
                print("Wrong operation!")
