import json


def read_data(path: str) -> list:
    try:
        with open(path, "r") as f:
            return json.load(f)
    except FileNotFoundError:
        return []


def write_data(data: list, path: str) -> None:
    with open(path, "w") as f:
        json.dump(data, f)
