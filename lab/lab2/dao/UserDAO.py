from lab.lab2.dao.IO import read_data, write_data


def get_user(login: str) -> dict:
    for user in get_users_list():
        if user["login"] == login:
            return user


def get_users_list() -> list:
    return read_data("data.json")


def update_user(user: dict) -> None:
    users = get_users_list()
    for i in range(len(users)):
        if users[i]["login"] == user["login"]:
            users[i] = user
            write_data(users, "data.json")


def create_user(user: dict) -> None:
    users = get_users_list()
    users.append(user)
    write_data(users, "data.json")
